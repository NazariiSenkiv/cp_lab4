package com.lab4.text;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TextFileReader {
    private String fileName = null;

    public TextFileReader(String fileName) {
        this.fileName = fileName;
    }

    public Text read() {
        Text text = new Text();

        try (Scanner fileScanner = new Scanner(new File(fileName))) {
            fileScanner.useDelimiter("\\.");

            while (fileScanner.hasNext()) {
                StringBuilder st = new StringBuilder(fileScanner.next());
                st.append(".");

                text.addSentence(st);
            }

        } catch (FileNotFoundException e) {
            System.out.println("File reading exception");
            throw new RuntimeException(e);
        }

        return text;
    }
}
