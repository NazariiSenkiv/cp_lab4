package com.lab4.text;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Text {
    private List<StringBuilder> sentences= new ArrayList<>();

    public Text() { }
    public Text(Text text) {
        for (var str: text.sentences) {
            sentences.add(new StringBuilder(str.toString()));
        }
    }

    public void addSentence(StringBuilder sentence) {
        sentences.add(sentence);
    }
    public List<StringBuilder> getSentences() {
        return sentences;
    }

    public int getSentencesCount() {
        return sentences.size();
    }

    /**
     * @param sentenceNum - number of sentence to find first word
     * @return sentence. If sentenceNum is wrong returns null
     */
    public StringBuilder getSentence(int sentenceNum) {
        // check the sentenceNum
        if (sentenceNum < 0 || sentenceNum > sentences.size()) {
            return null;
        }
        return sentences.get(sentenceNum);
    }

    /**
     * @param sentenceNum - number of sentence to find first word
     * @return first word. If sentenceNum is wrong returns null
     */
    public String getFirstWordInSentence(int sentenceNum) {
        // check the sentenceNum
        if (sentenceNum < 0 || sentenceNum > sentences.size()) {
            return null;
        }

        StringBuilder sentence = sentences.get(sentenceNum);

        String firstWord = null;

        try (Scanner wordScanner = new Scanner(sentence.toString())) {
            wordScanner.useDelimiter(" ");

            firstWord = wordScanner.next().trim();
        }

        return firstWord;
    }
}
