package com.lab4.text;

import com.lab4.EnUaDictionary;

import java.util.List;

public class TextManager {
    private final Text text;
    EnUaDictionary dictionary;

    public TextManager(Text text, EnUaDictionary dictionary) {
        this.text = text;
        this.dictionary = dictionary;
    }

    public Text replaceDictionaryWordsByFirstInSentence() {
        Text editedText = new Text(text);

        for (int i = 0; i < editedText.getSentencesCount(); ++i) {
            StringBuilder sentence = editedText.getSentence(i);

            StringBuilder sentenceLowerCase = new StringBuilder(sentence.toString().toLowerCase());

            List<String> allWords = dictionary.getEnglishWords();
            allWords.addAll(dictionary.getUkrainianWords());

            for (var enWord: allWords) {
                int substringId = 0;
                int startFrom = 0;

                while ((substringId = sentenceLowerCase.indexOf(enWord, startFrom)) != -1) {
                    startFrom = substringId+enWord.length();

                    sentence.replace(substringId, startFrom, editedText.getFirstWordInSentence(i));

                    // change also lowercase sentence to keep actual length
                    sentenceLowerCase.replace(substringId, startFrom, editedText.getFirstWordInSentence(i));
                }
            }
        }

        return editedText;
    }
}
