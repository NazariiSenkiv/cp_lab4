package com.lab4;

import com.lab4.text.Text;
import com.lab4.text.TextFileReader;
import com.lab4.text.TextManager;

public class Main {
    public static void main(String[] args) {
        EnUaDictionary dictionary = new EnUaDictionary("dictionary.txt");

        TextFileReader enTextFileReader = new TextFileReader("eng.txt");
        Text enText = enTextFileReader.read();

        TextFileReader uaTextFileReader = new TextFileReader("ukr.txt");
        Text uaText = uaTextFileReader.read();

        TextManager enTextManager = new TextManager(enText, dictionary);
        Text editedEn = enTextManager.replaceDictionaryWordsByFirstInSentence();

        TextManager uaTextManager = new TextManager(uaText, dictionary);
        Text editedUa = uaTextManager.replaceDictionaryWordsByFirstInSentence();

        System.out.println("\n======== Dictionary ========");
        printEnUaDictionary(dictionary);
        System.out.println("===========================\n");

        System.out.println("\n=== Original English text ===");
        printText(enText);
        System.out.println("\n\n=== Edited English text ===");
        printText(editedEn);
        System.out.println("\n===========================\n\n\n");

        System.out.println("\n=== Original Ukrainian text ===");
        printText(uaText);
        System.out.println("\n\n=== Edited Ukrainian text ===");
        printText(editedUa);
        System.out.println("\n===========================");
    }

    public static void printText(Text text) {
        for (var sentence: text.getSentences()) {
            System.out.print(sentence);
        }
    }

    public static void printEnUaDictionary(EnUaDictionary dictionary) {
        for (var enWord: dictionary.getEnglishWords()) {
            System.out.println(enWord + " => " + dictionary.getTranslation(enWord));
        }
    }
}