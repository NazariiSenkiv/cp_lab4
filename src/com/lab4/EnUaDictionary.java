package com.lab4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class EnUaDictionary {
    private final List<String> enWords = new ArrayList<>();
    private final List<String> uaWords = new ArrayList<>();

    public  EnUaDictionary(String filename) {
        try (Scanner scanner = new Scanner(new File(filename))) {
            while (scanner.hasNext()) {
                String row = scanner.nextLine();

                try (Scanner rowScanner = new Scanner(row)) {
                    rowScanner.useDelimiter("=");

                    String enWord = rowScanner.next().toLowerCase().trim();
                    String uaWord = rowScanner.next().toLowerCase().trim();

                    enWords.add(enWord);
                    uaWords.add(uaWord);
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("File reading failed!");
            throw new RuntimeException(e);
        }
    }

    public String getTranslation(String word) {
        String result = null;

        // try to find in english to ukrainian dictionary
        if (enWords.contains(word)) {
            result = uaWords.get(enWords.indexOf(word));
        } // try to find in ukrainian to english dictionary
        else if (uaWords.contains(word)) {
            result = enWords.get(uaWords.indexOf(word));
        }

        return result;
    }

    public List<String> getUkrainianWords() {
        return new ArrayList<>(uaWords);
    }
    public List<String> getEnglishWords() {
        return new ArrayList<>(enWords);
    }
}
